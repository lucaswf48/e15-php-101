<!-- Implemente uma página em PHP que imprima a seguinte frase: "Olá terráqueo, agora são [hh:mm] horas". A data e hora deverão ser recuperadas do próprio servidor, por meio do objeto Date. -->

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>Questão 1</title>
	</head>

	<body>
		
		<?php

			$data = date('H:i');
			echo "Olá terráqueo, agora são $data horas";
		

		?>
	
	</body>
</html>